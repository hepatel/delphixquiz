-Ensure Ruby is installed on your system
-clone or download the repository
(git clone https://hepatel@bitbucket.org/hepatel/delphixquiz.git)
-navigate to the root folder of the repository
-on the command prompt or terminal please run below command to install third party library used in the code

gem install 'rest-client'

-use below command to run the code in the test.rb file(this file contains the test examples with data)

ruby test.rb