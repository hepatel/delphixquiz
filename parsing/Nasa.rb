require 'rest-client'
require 'json'

class NASA

  def flyby(longitude, latitude)
    puts "you have entered below values"
    puts "longitude: #{longitude}"
    puts "latitude: #{latitude}"
    apikey = "9Jz6tLIeJ0yY9vjbEUWaH9fsXA930J9hspPchute"


    if(latitude < -90 || latitude > 90 )
      puts "latitude entered is not in the correct range, please enter correct latitude ranging from 0 and 90"
      return

    elsif(longitude < -180 || longitude >180)
      puts "longitude entered is not in the correct range, please enter correct longitude ranging from 0 and 180"
      return
    end

    begin
      response = RestClient.get("https://api.nasa.gov/planetary/earth/assets?lat=#{latitude}&lon=#{longitude}&api_key=#{apikey}")
    rescue RestClient::ExceptionWithResponse => err
      puts "something went wrong API and hence could not fetch the response"
      puts "error code is #{err.response.code}"

      if(err.response.code == 403)
        puts "access to API is deined please check the API key"
      end

      return
    end

    myJson = JSON.parse(response.body)
    # puts myJson

    if(myJson["count"]<2)
      puts "API response does not contain enough information to predict the future time"
      return
    end

    dates = Array.new

    # converting Dates(string) into DateTime object
    for i in myJson["results"]
      # puts i['date']
      d = DateTime.strptime(i['date'],'%Y-%m-%dT%H:%M:%S' )
      dates.push(d)
    end

    delta_days = Array.new

    for i in 0 ... dates.length-1
      delta = (dates[i+1] - dates[i])
      delta_days.push(delta.to_i)
    end

    sum = 0
    for j in delta_days
      sum = sum + j
    end

    avg_time_delta = sum / delta_days.length
    puts "avg_time_delta is: #{avg_time_delta}"

    last_date = dates.last.to_s
    puts "last date is: #{last_date}"

    next_time = dates.last + avg_time_delta
    puts "next time: #{next_time}"

  end

end

