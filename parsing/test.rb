require_relative 'Nasa'

s = NASA.new

puts "\n------------first test---------\n"
s.flyby(100.75,1.5)

puts "\n------------Grand Canyon--------\n"
s.flyby(-112.097796, 36.098592) #Grand Canyon

puts "\n--------Niagara Falls-----------\n"
s.flyby(-79.075891, 43.078154) #Niagara Falls

puts "\n--------our Corners Monument-----------\n"
s.flyby(-109.045183, 36.998979) # Four Corners Monument
